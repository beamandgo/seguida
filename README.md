# SEGUIDA README

## Background

A migrant worker from the Philippines. Because of economic hardship, she works in Singapore, making $800 dollars a month. Most of that money is remitted back home to support her family.

After her contract is completed, she goes back to her home, her kids have dropped out of school, her husband is nowhere to be found, no money is saved.

What happened to the money? This is a typical migrant worker story.

A migrant worker supports her family by sending cash. And cash is great. It can be used to buy food, medicine and pay bills. But cash can also be abused for gambling, drugs, and alcohol. In these cases, the hard work and is wasted.

At BeamAndGo that’s the problem that we solve.

[BeamAndGo](https://www.beamandgo.com) enables and educates migrant workers to achieve family resilience and security through an inclusive digital m,marketplace and financial ecosystem. Our customers are overseas Filipino workers (or OFWs) These are migrant workers who originate from the Philippines but now live and work in countries like Singapore, Hong Kong, Taiwan, Japan, the US, EU and middle East.

## The Project

The purpose of SEGUIDA is to define and create relationships and actions between the migrant worker and their beneficiaries – spouse, children, parents, siblings, and friends.

SEGUIDA provides a configurable, customizable, and extendable platform for managing social relationships. It will be launched with an API,  a web-based management tool and all related database schema and libraries.

With SEGUIDA, BeamAndGo members can add family members as beneficiaries, create a hierarchy of financial controls within the family, allocate funds to family members, and control spending on a category level.

The value to OFWs translates to increased savings, financial literacy, and a path to re-integration into the Philippine society.

For their beneficiaries, their necessities will be met, their children will attend school, and with the economic stability, our OFWs will spend less time away from home, resulting in closer-knit families.

For the Philippines, the value is the achievement of overall development plans, reduce future economic, environmental, and social costs, and reduce poverty.

## Components

Project SEGUIDA is made up of three (3) major components:

| Component | Repository | Description |
| --------- | ---------- | ----------- |
| Library   | https://gitlab.com/beamandgo/v2.0/member-lib-2.0 | Service and Repository Layer containing business logic and domain models. |
| Web       | https://gitlab.com/beamandgo/v2.0/member-web-2.0 | Web frontend and REST APIs for self-service. |
| Admin     | https://gitlab.com/beamandgo/v2.0/member-admin-2.0 | Admin frontend and REST APIs for administrative management. |

## Install

TBD

## Configuration

TBD

## Usage

TBD

## Security

TBD

## API

APIs are available via Swagger UI. Live sample is available at:
https://member.qa.beamandgo.com/swagger/index.html

## Road Map

The public version release for open source code is available and represents Phase 1 of Project SEGUIDA. We are grateful for being able to build this phase within the UNICEF and ING's Fintech for Impact program. 

With their support, we were able to build SEGUIDA and use the code as a means to contribute the United Nation's Sustainable Development Goals (https://www.un.org/sustainabledevelopment/) ...

Goal #1. No poverty

Goal #4. Quality Education

Goal #5. Gender Equality

Goal #8. Decent Work and Economic Growth

Goal #9. Industry, Innovation, and Infrastructure

Goal #10. Reduced Inequalities

This is just the beginning.

As we are consuming the SEGUIDA APIs, we are putting together a wishlist (and a bug list) on what's next. This list will grow. It will ebb and flow. It will be SEGUIDA's "road map". Stay tuned for this list, you can view it here https://gitlab.com/beamandgo/seguida/-/issues.

And if you would like to contribute, please do. We welcome pull requests, ideas, insights, and any and all perspectives.

To find out more about contributing, see below or visit [CONTRIBUTING.md](CONTRIBUTING.md).


## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

For more info, see [CONTRIBUTING.md](CONTRIBUTING.md).


## Maintainers

- Jonathan E. Chua: jonathan.chua@beamandgo.com
- Justin Lee: justin.lee@beamandgo.com
- Nan Zarchi Kyaw: nan.kz@beamandgo.com

## License

This project is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html) open source license. Please refer to [LICENSE.md](LICENSE.md) for the full terms.

## Authors and Acknowledgement

Authors: Jonathan E. Chua and Justin Lee

Special acknowledgement and thanks to Abigail "AbbyCabs" Cabunoc for guiding us on our first open source project.


